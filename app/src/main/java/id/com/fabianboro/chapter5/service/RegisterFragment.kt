package id.com.fabianboro.chapter5.service

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.widget.doAfterTextChanged
import androidx.navigation.fragment.findNavController
import id.com.fabianboro.chapter5.R
import id.com.fabianboro.chapter5.database.Account
import id.com.fabianboro.chapter5.database.AccountDatabase
import id.com.fabianboro.chapter5.databinding.FragmentRegisterBinding
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class RegisterFragment : Fragment() {
    private var _binding : FragmentRegisterBinding? = null
    private val binding get() = _binding!!

    private var mDb: AccountDatabase? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentRegisterBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mDb = AccountDatabase.getInstance(requireContext())
        checkConfirmPasswordOnChange()
        registerBtnClicked()
    }

    private fun checkConfirmPasswordOnChange() {
        binding.etPasswordConfirm.doAfterTextChanged {
            if (checkConfirmPassword()){
                binding.etPasswordConfirm.error = null
            } else {
                binding.etPasswordConfirm.error = "Password tidak sesuai"
            }
        }
    }

    private fun checkConfirmPassword() : Boolean {
        binding.apply {
            return etPassword.text.toString() == etPasswordConfirm.text.toString()
        }
    }

    private fun registerBtnClicked() {
        binding.btnRegister.setOnClickListener {
            registerAction()
        }
    }

    private fun registerAction() {
        binding.apply {
            val username = etUsername.text.toString()
            val email = etEmail.text.toString()
            val password = etPassword.text.toString()
            val passwordConfirm = etPasswordConfirm.text.toString()
            when {
                username.isEmpty() -> {
                    etUsername.error = "Username harus diisi"
                }
                email.isEmpty() -> {
                    etEmail.error = "Email harus diisi"
                }
                password.isEmpty() -> {
                    etPassword.error = "Password harus diisi"
                }
                passwordConfirm.isEmpty() -> {
                    etPasswordConfirm.error = "Password harus diisi"
                }
                password!= passwordConfirm -> {
                    etPasswordConfirm.error = "Password tidak sesuai"
                }
                else -> {
                    checkRegisteredEmail(username, email, password)
                }
            }

        }
    }

    private fun saveToDb(username: String, email: String, password: String) {
        val account = Account(null, username, email, password)
        CoroutineScope(Dispatchers.IO).launch {
            val result = mDb?.accountDao()?.insertAccount(account)
            if(result != 0L) {
                showToastInMainThread("Data Akun Berhasil Ditambah")
                CoroutineScope(Dispatchers.Main).launch {
                    findNavController().popBackStack()
                }
            } else {
                showToastInMainThread("Data Akun Gagal Ditambah")
            }
        }
    }

    private fun checkRegisteredEmail(username: String, email: String, password: String) {
        CoroutineScope(Dispatchers.IO).launch {
            val result = mDb?.accountDao()?.getRegisteredAccountEmail(email)
            if (!result.isNullOrEmpty()) {
                showToastInMainThread("Email Sudah Terdaftar")
            } else {
                saveToDb(username, email, password)
            }
        }
    }

    private fun showToastInMainThread(message: String) {
        CoroutineScope(Dispatchers.Main).launch {
            Toast.makeText(requireContext(),message,Toast.LENGTH_SHORT).show()
        }
    }
}
