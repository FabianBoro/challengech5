package id.com.fabianboro.chapter5.service

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.com.fabianboro.chapter5.R


class LoginFragment : Fragment() {
    private var _binding : FragmentLoginBinding? = null
    private val binding get() = _binding!!

    private lateinit var sharedPreferences: SharedPreferences
    private var mDb: AccountDatabase? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sharedPreferences = requireContext().getSharedPreferences("login",Context.MODE_PRIVATE)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentLoginBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mDb = AccountDatabase.getInstance(requireContext())
        tvToRegisterClicked()
        btnLoginClicked()
    }

    private fun btnLoginClicked() {
        binding.apply {
            btnLogin.setOnClickListener {
                val email = etEmail.text.toString()
                val password = etPassword.text.toString()
                when {
                    email.isEmpty() -> {
                        etEmail.error = "Email harus diisi"
                    }
                    password.isEmpty() -> {
                        etPassword.error = "Password harus diisi"
                    }
                    else -> {
                        loginAction(email, password)
                    }
                }
            }
        }
    }

    private fun loginAction(email: String?, password: String?) {
        CoroutineScope(Dispatchers.IO).launch {
            val result = mDb?.accountDao()?.getRegisteredAccount(email,password)
            if (!result.isNullOrEmpty()) {
                showToastInMainThread("Login Berhasil")
                val editor = sharedPreferences.edit()
                editor.putInt("accountId", result[0].id!!)
                editor.putString("username", result[0].username)
                editor.putBoolean("isLogin", true)
                editor.apply()
                CoroutineScope(Dispatchers.Main).launch {
                    findNavController().navigate(R.id.action_loginFragment_to_homeFragment)
                }
            } else if (email != null && password != null) {
                showToastInMainThread("Username atau Password Salah")
            }
        }
    }

    private fun tvToRegisterClicked() {
        binding.apply {
            tvToRegister.setOnClickListener {
                it.findNavController().navigate(R.id.action_loginFragment_to_registerFragment)
            }
        }
    }

    private fun showToastInMainThread(message: String) {
        CoroutineScope(Dispatchers.Main).launch {
            Toast.makeText(requireContext(),message,Toast.LENGTH_SHORT).show()
        }
    }
}
