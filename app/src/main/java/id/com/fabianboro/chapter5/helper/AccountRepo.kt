package id.com.fabianboro.chapter5.helper

import android.content.Context
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import androidx.lifecycle.MutableLiveData
import id.com.fabianboro.chapter5.database.Account
import id.com.fabianboro.chapter5.database.AccountDatabase

class AccountRepo(context: Context) {
    private val mDb = AccountDatabase.getInstance(context)

    suspend fun getRegisteredAccount(email: String?, password: String?) =
        withContext(Dispatchers.IO) {
            mDb?.accountDao()?.getRegisteredAccount(email, password)
        }

    suspend fun getRegisteredAccountEmail(email: String?) = withContext(Dispatchers.IO) {
        mDb?.accountDao()?.getRegisteredAccountEmail(email)
    }

    suspend fun getRegisteredAccountId(id: Int) = withContext(Dispatchers.IO) {
        mDb?.accountDao()?.getRegisteredAccountId(id)
    }

    suspend fun insertAccount(account: Account) = withContext(Dispatchers.IO) {
        mDb?.accountDao()?.insertAccount(account)
    }

    suspend fun updateProfileAccount(
        id: Int,
        username: String,
        fullname: String,
        birthdate: String,
        address: String
    ) = withContext(Dispatchers.IO) {
        mDb?.accountDao()?.updateProfileAccount(id, username, fullname, birthdate, address)
    }
}
