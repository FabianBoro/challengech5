package id.com.fabianboro.chapter5.service.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import id.com.fabianboro.chapter5.BuildConfig
import id.com.fabianboro.chapter5.model.DetailMovie
import id.com.fabianboro.chapter5.service.ApiService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DetailMovieViewModel(private val apiService: ApiService) : ViewModel() {
    private var _detailMovie = MutableLiveData<DetailMovie>()
    val detailMovie: LiveData<DetailMovie> get() = _detailMovie

    val errorMessage = MutableLiveData<String>()

    fun getDataFromNetwork(movieId: Int) {
        apiService.getDetailMovie(movieId, BuildConfig.API_KEY).enqueue(object :
            Callback<DetailMovie> {
            override fun onResponse(call: Call<DetailMovie>, response: Response<DetailMovie>) {
                if (response.isSuccessful) {
                    _detailMovie.postValue(response.body())
                } else {
                    errorMessage.value = response.errorBody().toString()
                }
            }

            override fun onFailure(call: Call<DetailMovie>, t: Throwable) {
                errorMessage.value = t.toString()
            }

        })
    }
}