package id.com.fabianboro.chapter5.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Account(
    @PrimaryKey(autoGenerate = true) val id: Int?,
    @ColumnInfo(name = "username") val username: String,
    @ColumnInfo(name = "email") val email: String,
    @ColumnInfo(name = "password") val password: String,
    @ColumnInfo(name = "fullname") val fullname: String,
    @ColumnInfo(name = "birthdate") val birthdate: String,
    @ColumnInfo(name = "address") val address: String

)
