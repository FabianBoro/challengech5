package id.com.fabianboro.chapter5.service.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import id.com.fabianboro.chapter5.helper.AccountRepo
import kotlinx.coroutines.launch

class LoginViewModel(private val accountRepo: AccountRepo) : ViewModel() {
    val accountRegistered = MutableLiveData<Boolean>()
    val usernamePref = MutableLiveData<String>()
    val accountIdPref = MutableLiveData<Int>()
    val emailPref = MutableLiveData<String>()

    fun loginAction(email: String?, password: String?) {
        viewModelScope.launch {
            val result = accountRepo.getRegisteredAccount(email, password)
            if (!result.isNullOrEmpty()) {
                accountIdPref.value = result[0].id!!
                usernamePref.value = result[0].username
                emailPref.value = result[0].email
                accountRegistered.value = true
            } else if (email != null && password != null) {
                accountRegistered.value = false
            }
        }
    }
}