package id.com.fabianboro.chapter5.service

import id.com.fabianboro.chapter5.model.DetailMovie
import id.com.fabianboro.chapter5.model.PopularMovie
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {
    @GET("movie/popular")
    fun getPopularMovie(@Query("api_key") key: String): Call<PopularMovie>

    @GET("movie/{movie_id}")
    fun getDetailMovie(@Path("movie_id") id: Int, @Query("api_key") key: String): Call<DetailMovie>
}
