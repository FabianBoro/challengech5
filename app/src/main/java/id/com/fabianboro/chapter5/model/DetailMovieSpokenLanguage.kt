package id.com.fabianboro.chapter5.model

import com.google.gson.annotations.SerializedName

data class DetailMovieSpokenLanguage(
    @SerializedName("iso_639_1")
    val iso6391: String,
    @SerializedName("name")
    val name: String
)
