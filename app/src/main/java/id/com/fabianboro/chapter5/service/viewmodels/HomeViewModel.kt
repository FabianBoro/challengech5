package id.com.fabianboro.chapter5.service.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.viewbinding.BuildConfig
import id.com.fabianboro.chapter5.model.PopularMovie
import id.com.fabianboro.chapter5.model.ResultPopularMovie
import id.com.fabianboro.chapter5.service.ApiService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class HomeViewModel(private val apiService: ApiService) : ViewModel() {
    private var _movies = MutableLiveData<List<ResultPopularMovie>>()
    val movie: LiveData<List<ResultPopularMovie>> get() = _movies

    private var _errorMessage = MutableLiveData<String>()
    val errorMessage: LiveData<String> get() = _errorMessage

    fun getDataFromNetwork() {
        apiService.getPopularMovie("498549d41b041783631ec3d45977d4c2").enqueue(object : Callback<PopularMovie> {
            override fun onResponse(call: Call<PopularMovie>, response: Response<PopularMovie>) {
                if (response.isSuccessful) {
                    _movies.postValue(response.body()?.resultPopularMovies)
                } else {
                    _errorMessage.postValue(response.errorBody().toString())
                }
            }

            override fun onFailure(call: Call<PopularMovie>, t: Throwable) {
                _errorMessage.postValue(t.toString())
            }
        })
    }

}